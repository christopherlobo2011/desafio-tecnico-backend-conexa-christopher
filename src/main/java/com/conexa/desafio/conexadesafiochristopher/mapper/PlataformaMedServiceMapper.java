package com.conexa.desafio.conexadesafiochristopher.mapper;

import com.conexa.desafio.conexadesafiochristopher.model.Atendimento;
import com.conexa.desafio.conexadesafiochristopher.model.Login;
import com.conexa.desafio.conexadesafiochristopher.model.Medico;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface PlataformaMedServiceMapper {

    void cadastrar(@Param("medico") Medico medico);

    boolean existeCadastro(@Param("email") String email);

    Medico recuperar(@Param("email") String email);

    void inserir(@Param("login") Login login);

    void deslogar(@Param("token") String token);

    boolean isTokenAtivo(@Param("token") String token);

    void inserirAtendimento(@Param("atendimento") Atendimento atendimento);

    Medico recuperarPor(@Param("token") String token);

}
