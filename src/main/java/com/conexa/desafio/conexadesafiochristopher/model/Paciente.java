package com.conexa.desafio.conexadesafiochristopher.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Paciente {

    private String nome;

    private String cpf;
}
