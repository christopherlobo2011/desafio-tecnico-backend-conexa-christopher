package com.conexa.desafio.conexadesafiochristopher.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Login {

    @JsonIgnore
    private Long id;
    @JsonIgnore
    private Long idMedico;

    private String token;

    private String tipo;
    @JsonIgnore
    private boolean ativo;

}
