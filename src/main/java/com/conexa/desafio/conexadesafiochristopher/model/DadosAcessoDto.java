package com.conexa.desafio.conexadesafiochristopher.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DadosAcessoDto {

    String email;

    String senha;
}
