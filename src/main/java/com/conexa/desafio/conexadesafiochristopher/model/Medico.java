package com.conexa.desafio.conexadesafiochristopher.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Medico {

    private Long id;

    private String email;

    private String senha;

    private String confirmacaoSenha;

    private String especialidade;

    private String cpf;

    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataNascimento;

    private String telefone;
}
