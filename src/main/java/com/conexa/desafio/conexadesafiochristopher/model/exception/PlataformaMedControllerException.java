package com.conexa.desafio.conexadesafiochristopher.model.exception;

public class PlataformaMedControllerException extends RuntimeException{

    public PlataformaMedControllerException(String mensagem){
        super(mensagem);
    }
}
