package com.conexa.desafio.conexadesafiochristopher.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@MapperScan("com.conexa.desafio.conexadesafiochristopher.mapper")
public class DBConfig {

}
