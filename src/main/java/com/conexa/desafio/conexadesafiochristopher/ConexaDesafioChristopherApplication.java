package com.conexa.desafio.conexadesafiochristopher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class ConexaDesafioChristopherApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConexaDesafioChristopherApplication.class, args);
    }
}
