package com.conexa.desafio.conexadesafiochristopher.service.interfaces;

import com.conexa.desafio.conexadesafiochristopher.model.DadosAcessoDto;
import com.conexa.desafio.conexadesafiochristopher.model.Login;

public interface AuthenticationService {

    Login logar(DadosAcessoDto login);
    void deslogar(String authorization);

}
