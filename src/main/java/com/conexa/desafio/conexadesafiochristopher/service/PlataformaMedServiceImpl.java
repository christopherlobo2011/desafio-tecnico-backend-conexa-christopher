package com.conexa.desafio.conexadesafiochristopher.service;

import br.com.caelum.stella.validation.CPFValidator;
import com.conexa.desafio.conexadesafiochristopher.mapper.PlataformaMedServiceMapper;
import com.conexa.desafio.conexadesafiochristopher.model.Atendimento;
import com.conexa.desafio.conexadesafiochristopher.model.Medico;
import com.conexa.desafio.conexadesafiochristopher.model.exception.PlataformaMedControllerException;
import com.conexa.desafio.conexadesafiochristopher.service.interfaces.PlataformaMedService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PlataformaMedServiceImpl implements PlataformaMedService {

    private final PlataformaMedServiceMapper mapper;
    private final TokenService tokenService;

    @Override
    public Medico cadastrar(Medico medico){

        this.validarMedico(medico);

        if (this.possuiCadastro(medico))
            throw new PlataformaMedControllerException("O médico que você está tentando inserir já possui cadastro");

        return  Optional.ofNullable(this.cadastrarMedico(medico)).orElseThrow(() ->
              new PlataformaMedControllerException("Não foi possível realizar o cadastro"));
    }

    @Override
    public Atendimento criarAtendimento(Atendimento atendimento, String authorization){

        String tokenTratado = authorization.replace("Bearer","");

        if(!this.validate(tokenTratado.trim()))
            throw new PlataformaMedControllerException("Token inválido");

        if(!this.isTokenAtivo(tokenTratado.trim()))
            throw new PlataformaMedControllerException("Token inativo");

        if(atendimento.getDataHora().isBefore(LocalDateTime.now()))
            throw new PlataformaMedControllerException("Você deve criar um agendamento com uma data futura");

        return  Optional.ofNullable(this.inserirAtendimento(atendimento,tokenTratado.trim())).orElseThrow(() ->
                new PlataformaMedControllerException("Não foi possível realizar o cadastro do atendimento"));
    }

    protected boolean validate(String token){
        Claims claims = tokenService.decodeToken(token);
        return !claims.getExpiration().before(new Date(System.currentTimeMillis()));
    }

    protected Medico cadastrarMedico(Medico medico){
        medico.setSenha(this.getPasswordEncoder(medico.getSenha()));
        medico.setConfirmacaoSenha(this.getPasswordEncoder(medico.getConfirmacaoSenha()));
        mapper.cadastrar(medico);
        return medico;
    }

    protected String getPasswordEncoder(String senha){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(senha);
    }

    protected boolean possuiCadastro(Medico medico){
        return mapper.existeCadastro(medico.getEmail());
    }

    protected void validarMedico(Medico medico){

        if(!medico.getSenha().equals(medico.getConfirmacaoSenha()))
            throw new PlataformaMedControllerException("As senhas devem ser iguais");
        if(!this.validaEmail(medico.getEmail()))
            throw new PlataformaMedControllerException("Email inválido");
        if(!this.validaCpf(medico.getCpf()))
            throw new PlataformaMedControllerException("Cpf inválido");
    }

    protected boolean validaEmail(String email){

        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    protected boolean validaCpf(String cpf){
        String cpfTratado = cpf.replaceAll("[^0-9]","");

        CPFValidator cpfValidator = new CPFValidator();
        try{
            cpfValidator.assertValid(cpfTratado.trim());
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    protected boolean isTokenAtivo(String token){
        return mapper.isTokenAtivo(token);
    }

    protected Atendimento inserirAtendimento(Atendimento atendimento, String authorization){
        atendimento.setIdMedico(this.recuperarMedico(authorization).getId());
        mapper.inserirAtendimento(atendimento);
        return atendimento;
    }

    protected Medico recuperarMedico(String authorization){
        return mapper.recuperarPor(authorization);
    }
}
