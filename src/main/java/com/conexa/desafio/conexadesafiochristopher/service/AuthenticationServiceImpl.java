package com.conexa.desafio.conexadesafiochristopher.service;

import com.conexa.desafio.conexadesafiochristopher.mapper.PlataformaMedServiceMapper;
import com.conexa.desafio.conexadesafiochristopher.model.DadosAcessoDto;
import com.conexa.desafio.conexadesafiochristopher.model.Login;
import com.conexa.desafio.conexadesafiochristopher.model.Medico;
import com.conexa.desafio.conexadesafiochristopher.model.exception.PlataformaMedControllerException;
import com.conexa.desafio.conexadesafiochristopher.service.interfaces.AuthenticationService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private static final String TIPO_TOKEN = "Bearer";

    private final TokenService tokenService;
    private final PlataformaMedServiceMapper mapper;

    @Override
    public Login logar(DadosAcessoDto dadosAcessoDto){
        Medico medico = Optional.ofNullable(this.recuperarMedico(dadosAcessoDto)).orElseThrow(() ->
                new PlataformaMedControllerException("Nenhum cadastro encontrado"));

        if(!this.validatePassword(dadosAcessoDto.getSenha(),medico.getSenha()))
            throw new PlataformaMedControllerException("As senhas informadas são divergentes");

        return Optional.ofNullable(this.realizarLogin(medico)).orElseThrow(() ->
                new PlataformaMedControllerException("Não foi possível realizar o login"));
    }

    @Override
    public void deslogar(String authorization){
        if(!this.validate(authorization))
            throw new PlataformaMedControllerException("Token inválido");

        this.realizarLogoff(authorization);
    }

    protected Medico recuperarMedico(DadosAcessoDto dadosAcessoDto){
        return mapper.recuperar(dadosAcessoDto.getEmail());
    }

    protected boolean validate(String token){
        String tokenTratado = token.replace(TIPO_TOKEN,"");
        Claims claims = tokenService.decodeToken(tokenTratado.trim());
        return !claims.getExpiration().before(new Date(System.currentTimeMillis()));
    }

    protected boolean validatePassword(String senhaDigitada, String senha){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.matches(senhaDigitada,senha);
    }

    protected Login montarLogin(Medico medico){
        return Login.builder()
                .tipo(TIPO_TOKEN)
                .token(tokenService.generateToken())
                .ativo(true)
                .idMedico(medico.getId()).build();
    }

    protected Login inserirLogin(Login login){
        mapper.inserir(login);
        return login;
    }

    protected void realizarLogoff(String token){
        String tokenTratado = token.replace(TIPO_TOKEN,"");
        mapper.deslogar(tokenTratado.trim());
    }

    protected Login realizarLogin(Medico medico){
        Login login = this.montarLogin(medico);
        this.inserirLogin(login);
        return login;
    }
}
