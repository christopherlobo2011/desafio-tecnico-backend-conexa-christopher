package com.conexa.desafio.conexadesafiochristopher.service.interfaces;

import com.conexa.desafio.conexadesafiochristopher.model.Atendimento;
import com.conexa.desafio.conexadesafiochristopher.model.Medico;

public interface PlataformaMedService {

    Medico cadastrar(Medico medico);
    Atendimento criarAtendimento(Atendimento atendimento, String authorization);
}
