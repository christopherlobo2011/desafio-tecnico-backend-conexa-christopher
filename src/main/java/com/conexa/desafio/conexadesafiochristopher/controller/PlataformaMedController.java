package com.conexa.desafio.conexadesafiochristopher.controller;

import com.conexa.desafio.conexadesafiochristopher.model.Atendimento;
import com.conexa.desafio.conexadesafiochristopher.model.Medico;
import com.conexa.desafio.conexadesafiochristopher.service.interfaces.PlataformaMedService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")

public class PlataformaMedController {

    final PlataformaMedService plataformaMedService;

    @PostMapping("/v1/signup")
    public ResponseEntity<Medico> signup(@RequestBody Medico medico) {
        return ResponseEntity.ok(plataformaMedService.cadastrar(medico));
    }

    @PostMapping("/v1/attendance")
    public ResponseEntity<Atendimento> attendance(@RequestBody Atendimento atendimento, @RequestHeader String authorization) {
        return ResponseEntity.ok(plataformaMedService.criarAtendimento(atendimento, authorization));
    }
}
