package com.conexa.desafio.conexadesafiochristopher.controller;

import com.conexa.desafio.conexadesafiochristopher.model.DadosAcessoDto;
import com.conexa.desafio.conexadesafiochristopher.model.Login;
import com.conexa.desafio.conexadesafiochristopher.service.interfaces.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")

public class AuthenticationController {

    final AuthenticationService authenticationService;

    @PostMapping("/v1/login")
    public ResponseEntity<Login> login(@RequestBody DadosAcessoDto dadosAcessoDto) {
        return ResponseEntity.ok(authenticationService.logar(dadosAcessoDto));
    }

    @PostMapping("/v1/logoff")
    public ResponseEntity<Void> logoff(@RequestHeader String authorization) {
        authenticationService.deslogar(authorization);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
