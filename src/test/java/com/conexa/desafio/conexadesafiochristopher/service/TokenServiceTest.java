package com.conexa.desafio.conexadesafiochristopher.service;

import org.junit.Assert;
import org.junit.Test;

public class TokenServiceTest {
    TokenService tokenService = new TokenService();

    @Test
    public void testGenerateToken(){
        String result = tokenService.generateToken();
        String expected = result;
        Assert.assertEquals(expected,result);
    }
}
