package com.conexa.desafio.conexadesafiochristopher.service;

import com.conexa.desafio.conexadesafiochristopher.mapper.PlataformaMedServiceMapper;
import com.conexa.desafio.conexadesafiochristopher.model.DadosAcessoDto;
import com.conexa.desafio.conexadesafiochristopher.model.Login;
import com.conexa.desafio.conexadesafiochristopher.model.Medico;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.Month;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

public class AuthenticationServiceImplTest {
    @Mock
    TokenService tokenService;
    @Mock
    PlataformaMedServiceMapper mapper;
    @InjectMocks
    AuthenticationServiceImpl authenticationServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRecuperarMedico() throws Exception {
        when(mapper.recuperar(anyString())).thenReturn(new Medico(Long.valueOf(1), "email", "senha", "confirmacaoSenha", "especialidade", "cpf", LocalDate.of(2022, Month.APRIL, 4), "telefone"));

        Medico result = authenticationServiceImpl.recuperarMedico(new DadosAcessoDto("email", "senha"));
        Assert.assertEquals(new Medico(Long.valueOf(1), "email", "senha", "confirmacaoSenha", "especialidade", "cpf", LocalDate.of(2022, Month.APRIL, 4), "telefone"), result);
    }

    @Test
    public void testValidatePassword() throws Exception {
        boolean result = authenticationServiceImpl.validatePassword("senhaDigitada", "senha");
        Assert.assertEquals(false, result);
    }

    @Test
    public void testMontarLogin() throws Exception {
        when(tokenService.generateToken()).thenReturn("generateTokenResponse");

        Login result = authenticationServiceImpl.montarLogin(new Medico(Long.valueOf(1), "email", "senha", "confirmacaoSenha", "especialidade", "cpf", LocalDate.of(2022, Month.APRIL, 4), "telefone"));
        Assert.assertEquals(new Login(null, Long.valueOf(1), "generateTokenResponse", "Bearer", true), result);
    }

    @Test
    public void testInserirLogin() throws Exception {
        Login result = authenticationServiceImpl.inserirLogin(new Login(Long.valueOf(1), Long.valueOf(1), "token", "tipo", true));
        Assert.assertEquals(new Login(Long.valueOf(1), Long.valueOf(1), "token", "tipo", true), result);
    }

    @Test
    public void testRealizarLogoff() throws Exception {
        authenticationServiceImpl.realizarLogoff("token");
    }

    @Test
    public void testRealizarLogin() throws Exception {
        when(tokenService.generateToken()).thenReturn("generateTokenResponse");

        Login result = authenticationServiceImpl.realizarLogin(new Medico(Long.valueOf(1), "email", "senha", "confirmacaoSenha", "especialidade", "cpf", LocalDate.of(2022, Month.APRIL, 4), "telefone"));
        Assert.assertEquals(new Login(null, Long.valueOf(1), "generateTokenResponse", "Bearer", true), result);
    }
}
