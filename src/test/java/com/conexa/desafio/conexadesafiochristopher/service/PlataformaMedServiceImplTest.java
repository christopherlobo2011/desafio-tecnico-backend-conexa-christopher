package com.conexa.desafio.conexadesafiochristopher.service;

import com.conexa.desafio.conexadesafiochristopher.mapper.PlataformaMedServiceMapper;
import com.conexa.desafio.conexadesafiochristopher.model.Atendimento;
import com.conexa.desafio.conexadesafiochristopher.model.Medico;
import com.conexa.desafio.conexadesafiochristopher.model.Paciente;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import static org.mockito.Mockito.*;

public class PlataformaMedServiceImplTest {
    @Mock
    PlataformaMedServiceMapper mapper;
    @Mock
    TokenService tokenService;
    @InjectMocks
    PlataformaMedServiceImpl plataformaMedServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCadastrarMedico() throws Exception {
        Medico result = plataformaMedServiceImpl.cadastrarMedico(new Medico(Long.valueOf(1), "email", "senha", "confirmacaoSenha", "especialidade", "cpf", LocalDate.of(2022, Month.APRIL, 4), "telefone"));
        Medico expected = result;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testGetPasswordEncoder() throws Exception {
        String result = plataformaMedServiceImpl.getPasswordEncoder("senha");
        String expected = result;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testPossuiCadastro() throws Exception {
        when(mapper.existeCadastro(anyString())).thenReturn(true);

        boolean result = plataformaMedServiceImpl.possuiCadastro(new Medico(Long.valueOf(1), "email", "senha", "confirmacaoSenha", "especialidade", "cpf", LocalDate.of(2022, Month.APRIL, 4), "telefone"));
        Assert.assertEquals(true, result);
    }

    @Test
    public void testValidarMedico() throws Exception {
        plataformaMedServiceImpl.validarMedico(new Medico(Long.valueOf(1), "andersson@gmail.com", "senha", "senha", "especialidade", "05417387398", LocalDate.of(2022, Month.APRIL, 4), "telefone"));
    }

    @Test
    public void testValidaEmail() throws Exception {
        boolean result = plataformaMedServiceImpl.validaEmail("email");
        Assert.assertEquals(false, result);
    }

    @Test
    public void testValidaCpf() throws Exception {
        boolean result = plataformaMedServiceImpl.validaCpf("05417387398");
        Assert.assertEquals(true, result);
    }

    @Test
    public void testIsTokenAtivo() throws Exception {
        when(mapper.isTokenAtivo(anyString())).thenReturn(true);

        boolean result = plataformaMedServiceImpl.isTokenAtivo("token");
        Assert.assertEquals(true, result);
    }

    @Test
    public void testInserirAtendimento() throws Exception {
        when(mapper.recuperarPor(anyString())).thenReturn(new Medico(Long.valueOf(1), "email", "senha", "confirmacaoSenha", "especialidade", "cpf", LocalDate.of(2022, Month.APRIL, 4), "telefone"));

        Atendimento result = plataformaMedServiceImpl.inserirAtendimento(new Atendimento(Long.valueOf(1), Long.valueOf(1), LocalDateTime.of(2022, Month.APRIL, 4, 23, 37, 40), new Paciente("nome", "cpf")), "authorization");
        Assert.assertEquals(new Atendimento(Long.valueOf(1), Long.valueOf(1), LocalDateTime.of(2022, Month.APRIL, 4, 23, 37, 40), new Paciente("nome", "cpf")), result);
    }

    @Test
    public void testRecuperarMedico() throws Exception {
        when(mapper.recuperarPor(anyString())).thenReturn(new Medico(Long.valueOf(1), "email", "senha", "confirmacaoSenha", "especialidade", "cpf", LocalDate.of(2022, Month.APRIL, 4), "telefone"));

        Medico result = plataformaMedServiceImpl.recuperarMedico("authorization");
        Assert.assertEquals(new Medico(Long.valueOf(1), "email", "senha", "confirmacaoSenha", "especialidade", "cpf", LocalDate.of(2022, Month.APRIL, 4), "telefone"), result);
    }
}