package com.conexa.desafio.conexadesafiochristopher.controller;

import com.conexa.desafio.conexadesafiochristopher.model.Atendimento;
import com.conexa.desafio.conexadesafiochristopher.model.Medico;
import com.conexa.desafio.conexadesafiochristopher.service.interfaces.PlataformaMedService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class PlataformaMedControllerTest {

    @Mock
    private PlataformaMedService service;

    @Mock
    Medico medico;

    @Mock
    Atendimento atendimento;

    @InjectMocks
    private PlataformaMedController controller;

    @Test
    public void deveRealizarCadastro(){
        medico = new Medico();
        Mockito.when(service.cadastrar(medico)).thenReturn(medico);
        ResponseEntity<Medico> expected = ResponseEntity.ok(medico);
        ResponseEntity<Medico> result = controller.signup(medico);
        Assertions.assertEquals(expected,result);
    }

    @Test
    public void deveRealizarAtendimento(){
        atendimento = new Atendimento();
        Mockito.when(service.criarAtendimento(atendimento, "")).thenReturn(atendimento);
        ResponseEntity<Atendimento> expected = ResponseEntity.ok(atendimento);
        ResponseEntity<Atendimento> result = controller.attendance(atendimento,"");
        Assertions.assertEquals(expected,result);
    }
}
