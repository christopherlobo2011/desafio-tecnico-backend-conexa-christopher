package com.conexa.desafio.conexadesafiochristopher.controller;

import com.conexa.desafio.conexadesafiochristopher.model.DadosAcessoDto;
import com.conexa.desafio.conexadesafiochristopher.model.Login;
import com.conexa.desafio.conexadesafiochristopher.service.interfaces.AuthenticationService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationControllerTest {

    @Mock
    private AuthenticationService service;

    @Mock
    Login login;

    @Mock
    DadosAcessoDto dadosAcessoDto;

    @InjectMocks
    private AuthenticationController controller;

    @Test
    public void deveRelizarLogin(){
        dadosAcessoDto = new DadosAcessoDto();
        login = new Login();
        Mockito.when(service.logar(dadosAcessoDto)).thenReturn(login);
        ResponseEntity<Login> expected = ResponseEntity.ok(login);
        ResponseEntity<Login> result = controller.login(dadosAcessoDto);
        Assertions.assertEquals(expected,result);
    }

    @Test
    public void deveRelizarLogoff(){
        ResponseEntity<Void> result = controller.logoff(Mockito.anyString());
        Mockito.verify(service, Mockito.times(1)).deslogar(Mockito.anyString());
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }
}
